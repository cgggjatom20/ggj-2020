﻿using UnityEngine;
using System.Collections;

namespace Game.Models
{
    [CreateAssetMenu(menuName = "Game/Player Input")]
    public class PlayerInput : ScriptableObject
    {
        public KeyCode forwardPositive;
        public KeyCode forwardNegative;
        public KeyCode rightPositive;
        public KeyCode rightNegative;

        public KeyCode interact;
        public KeyCode fire;
    }
}
