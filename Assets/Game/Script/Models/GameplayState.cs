﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Models
{
    [System.Serializable]
    public struct CompartmentData
    {
        public int compartmentId;
        public int damageAmount;
    }

    [CreateAssetMenu(menuName = "Game/Gameplay Data")]
    public class GameplayState : ScriptableObject
    {
        public float oxygen;
        public List<CompartmentData> compartments;
    }
}
