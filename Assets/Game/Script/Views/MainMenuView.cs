﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Views
{
    public class MainMenuView : MonoBehaviour
    {
        [SerializeField] private GameObject credits;

        public void StartGame()
        {
            gameObject.SetActive(false);

            var asyncOP = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            asyncOP.allowSceneActivation = true;
        }

        public void Credits()
        {
            if (credits)    credits.SetActive(true);
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
