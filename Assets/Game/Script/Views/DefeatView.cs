﻿using Game.Components.Ships;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Views
{
    public class DefeatView : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenu;
        [SerializeField] private TextMeshProUGUI progressText;

        private void OnEnable()
        {
            var ship = FindObjectOfType<Ship>();

            progressText.text = $"Your Progress: {Mathf.RoundToInt(ship.CurrentProgressPercentage() * 100).ToString()}%";
        }

        public void MainMenu()
        {
            var asyncOp = SceneManager.UnloadSceneAsync(1);
            asyncOp.allowSceneActivation = true;

            asyncOp.completed += OnComplete;


            void OnComplete(AsyncOperation op)
            {
                gameObject.SetActive(false);
                mainMenu.SetActive(true);
            }
        }

        public void PlayAgain()
        {
            var asyncOp = SceneManager.UnloadSceneAsync(1);
            asyncOp.allowSceneActivation = true;

            asyncOp.completed += OnComplete;


            void OnComplete(AsyncOperation op)
            {
                asyncOp.completed -= OnComplete;
                var loadOp = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
                loadOp.allowSceneActivation = true;

                loadOp.completed += OnLoaded;


                void OnLoaded(AsyncOperation loadOperation)
                {
                    loadOperation.completed -= OnLoaded;
                    SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
                    gameObject.SetActive(false);
                }
            }
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
