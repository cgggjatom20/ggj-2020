﻿using Game.Components.Ships;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game.Views
{
    public class TemperatureView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI temperatureLabel;
        [SerializeField] private TextMeshProUGUI temperatureCurrent;
        [SerializeField] private TextMeshProUGUI temperatureDelta;

        private Ship ship;

        private void Awake()
        {
            ship = FindObjectOfType<Ship>();

            ship.ProgressCompleted += OnGameOver;
            ship.TemperatureReachedZero += OnGameOver;
        }

        private void OnDestroy()
        {
            if (ship)
            {
                ship.ProgressCompleted -= OnGameOver;
                ship.TemperatureReachedZero -= OnGameOver;
            }
        }

        private void Update()
        {
            var currentTemperature = ship.CurrentTemperature();
            var deltaTemperature   = ship.TemperatureLossPerSecond();

            var textColor = Color.Lerp(Color.red, Color.white, ship.CurrentTemperaturePercentage() / 100f);

            temperatureCurrent.color = textColor;
            temperatureLabel.color   = textColor;
            temperatureDelta.color   = textColor;

            temperatureCurrent.text = $"{currentTemperature.ToString()}C";
            temperatureDelta.text   = $"{deltaTemperature.ToString()}C/s";

            ManageDeltaTextView(deltaTemperature > 0.01f);
        }

        private void ManageDeltaTextView(bool state)
        {
            if (state != temperatureDelta.gameObject.activeSelf)   temperatureDelta.gameObject.SetActive(state);
        }

        private void OnGameOver()
        {
            gameObject.SetActive(false);
        }
    }
}
