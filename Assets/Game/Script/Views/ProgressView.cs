﻿using Game.Components.Ships;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views
{
    public class ProgressView : MonoBehaviour
    {
        [SerializeField] private Image fillImage;

        private Ship ship;

        private void Awake()
        {
            ship = FindObjectOfType<Ship>();
        }

        private void Update()
        {
            fillImage.fillAmount = ship.CurrentProgressPercentage();
        }
    }
}
