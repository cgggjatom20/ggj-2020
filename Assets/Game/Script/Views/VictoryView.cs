﻿using Game.Components.Ships;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Views
{
    public class VictoryView : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenu;
        [SerializeField] private TextMeshProUGUI temperatureCurrent;
        [SerializeField] private TextMeshProUGUI temperatureText;

        private Ship ship;

        private void OnEnable()
        {
            ship = FindObjectOfType<Ship>();

            var currentTemperature = ship.CurrentTemperature();
            var deltaTemperature = ship.TemperatureLossPerSecond();

            var textColor = Color.Lerp(Color.red, Color.white, ship.CurrentTemperaturePercentage() / 100f);

            temperatureText.color = textColor;
            temperatureCurrent.color = textColor;

            temperatureCurrent.text = $"{currentTemperature.ToString()}C";
        }

        public void MainMenu()
        {
            var asyncOp = SceneManager.UnloadSceneAsync(1);
            asyncOp.allowSceneActivation = true;

            asyncOp.completed += OnComplete;


            void OnComplete(AsyncOperation op)
            {
                gameObject.SetActive(false);
                mainMenu.SetActive(true);
            }
        }

        public void PlayAgain()
        {
            var asyncOp = SceneManager.UnloadSceneAsync(1);
            asyncOp.allowSceneActivation = true;

            asyncOp.completed += OnComplete;


            void OnComplete(AsyncOperation op)
            {
                asyncOp.completed -= OnComplete;
                var loadOp = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
                loadOp.allowSceneActivation = true;

                loadOp.completed += OnLoaded;


                void OnLoaded(AsyncOperation loadOperation)
                {
                    loadOperation.completed -= OnLoaded;
                    SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
                    gameObject.SetActive(false);
                }
            }
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
