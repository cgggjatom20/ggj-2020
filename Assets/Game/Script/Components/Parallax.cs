﻿using Game.Components.Ships;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components
{
    public class Parallax : MonoBehaviour
    {
        [SerializeField] private Material material;
        [SerializeField] private float offsetPerSeconds;
        [SerializeField] private float startOffset;

        private Ship ship;
        private float offset;

        private void Awake()
        {
            ship = FindObjectOfType<Ship>();

            ship.StartedPlaying += OnStartedPlaying;
            ship.ProgressCompleted += OnProgressCompleted;

            enabled = false;

            offset += startOffset;
        }

        private void Update()
        {
            offset += Time.deltaTime * offsetPerSeconds;

            material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        }

        private void OnDestroy()
        {
            if (ship)
            {
                ship.StartedPlaying -= OnStartedPlaying;
                ship.ProgressCompleted -= OnProgressCompleted;
            }
        }

        private void OnStartedPlaying()
        {
            enabled = true;
        }

        private void OnProgressCompleted()
        {
            enabled = false;
        }
    }
}
