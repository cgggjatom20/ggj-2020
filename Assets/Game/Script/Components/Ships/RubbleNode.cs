﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components.Ships
{
    public class RubbleNode : MonoBehaviour
    {
        public event Action Collected;
        public event Action Scattered;

        [SerializeField] private List<Rubble> rubbles;

        private int scatteredCount;

        private void Awake()
        {
            foreach (var rubble in rubbles)
            {
                rubble.Collected += OnCollected;
            }
        }

        private void OnDestroy()
        {
            foreach (var rubble in rubbles)
            {
                if (rubble)
                {
                    rubble.Collected -= OnCollected;
                }
            }
        }

        public void Scatter(Vector3 scatterSourcePoint)
        {
            foreach (var rubble in rubbles)
            {
                rubble.Scatter(scatterSourcePoint);
            }

            scatteredCount = rubbles.Count;

            Damaged = true;

            Scattered?.Invoke();
        }

        private void OnCollected()
        {
            scatteredCount = Mathf.Clamp(scatteredCount - 1, 0, int.MaxValue);

            Damaged = scatteredCount > 0;

            if (!Damaged)
            {
                Collected?.Invoke();
            }
        }

        public bool Damaged { get; private set; }
    }
}
