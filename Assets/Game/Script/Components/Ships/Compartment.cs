﻿using Game;
using Game.Components;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components.Ships
{
    public class Compartment : MonoBehaviour
    {
        [SerializeField] private List<RubbleNode> rubbleNodes;

        private int damagedNodes;

        private void Awake()
        {
            foreach (var node in rubbleNodes)
            {
                node.Collected += OnNodeCollected;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (!enabled) return;

            var contact = collision.contacts[0];

            Impact(contact.point);
        }

        private void OnDestroy()
        {
            foreach (var node in rubbleNodes)
            {
                if (node)
                {
                    node.Collected -= OnNodeCollected;
                }
            }
        }

        private void Impact(Vector3 impactPosition)
        {
            // Find closest node to the point
            var closest = FindClosestNode(impactPosition);

            // If not damaged, damage it and scatter rubble
            if (!closest.node.Damaged)
            {
                closest.node.Scatter(impactPosition);
                damagedNodes++;
            }
        }

        private (int index, RubbleNode node) FindClosestNode(Vector3 point)
        {
            var closestNodeDistance = float.MaxValue;
            var closestNode         = (RubbleNode) default;
            var nodeIndex           = -1;

            for (int i = 0; i < rubbleNodes.Count; i++)
            {
                var node = rubbleNodes[i];

                var distance = Vector3.Distance(node.transform.position.XZ(), point.XZ());

                if (distance < closestNodeDistance)
                {
                    closestNodeDistance = distance;
                    closestNode = node;
                    nodeIndex = i;
                }
            }

            return (nodeIndex, closestNode);
        }

        [Button(ButtonSizes.Medium)]
        private void TestDamage(Transform testSource)
        {
            Impact(testSource.position);
        }

        [Button(ButtonSizes.Medium)]
        private void TestFullDamage()
        {
            foreach (var node in rubbleNodes)   Impact(node.transform.position);
        }

        private void OnNodeCollected()
        {
            damagedNodes = Mathf.Clamp(damagedNodes - 1, 0, int.MaxValue);
        }

        public float DamagePercent => damagedNodes / (float) rubbleNodes.Count;
    }
}
