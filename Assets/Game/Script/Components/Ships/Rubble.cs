﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components.Ships
{
    [RequireComponent(typeof(Rigidbody))]
    public class Rubble : MonoBehaviour
    {
        public event Action Collected;

        [SerializeField] private Transform initialTransform;
        [SerializeField] private float scatterPower;
        [SerializeField] private float scatterTorque;
        [SerializeField] private float repairDuration;

        private Rigidbody body;
        
        public bool Scattered { get; private set; }

        private void Awake()
        {
            body = GetComponent<Rigidbody>();
        }

        public void Scatter(Vector3 scatterSourcePoint)
        {
            if (Scattered) return;

            if (DOTween.IsTweening(transform))  transform.DOKill();

            var direction = (transform.position - scatterSourcePoint).normalized;

            body.AddForce(direction * scatterPower, ForceMode.Impulse);
            body.AddTorque(new Vector3(scatterTorque, scatterTorque, 0));

            Scattered = true;
        }

        [Button(ButtonSizes.Medium)]
        public void Collect()
        {
            if (!Scattered) return;

            transform.DOMove(initialTransform.position, repairDuration);
            transform.DORotate(initialTransform.rotation.eulerAngles, repairDuration)
                .OnComplete(() =>
                {
                    Scattered = false;
                    Collected?.Invoke();
                });
        }
    }
}
