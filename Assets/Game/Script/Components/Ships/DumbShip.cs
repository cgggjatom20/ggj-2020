﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components.Ships
{
    public class DumbShip : Ship
    {
        [SerializeField] private Compartment left;
        [SerializeField] private Compartment right;
        [SerializeField] private Compartment front;
        [SerializeField] private Compartment back;

        public override float TemperatureLossPerSecond()
        {
            var totalDamage = 0f;

            totalDamage += left.DamagePercent;
            totalDamage += right.DamagePercent;
            totalDamage += front.DamagePercent;
            totalDamage += back.DamagePercent;

            totalDamage /= 4;

            return totalDamage * MaxTemperatureLossPerSecond;
        }

        protected override void TemperatureAtZero()
        {
            base.TemperatureAtZero();

            // Other temperature reached zero effects, animations etc
        }
    }
}
