﻿using DG.Tweening;
using Game.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components.Ships
{
    public abstract class Ship : MonoBehaviour
    {
        public event Action TemperatureReachedZero;
        public event Action ProgressCompleted;
        public event Action StartedPlaying;

        [SerializeField] private float progressPerSeconds;
        [SerializeField] private int maxTemperature;
        [SerializeField] private int maxTemperatureLossPerSecond;
        [SerializeField] private Player player1;
        [SerializeField] private Player player2;
        [SerializeField] private Transform gameEndMoveTransform;
        [SerializeField] private Transform gameOverMoveTransform;

        private GameManager gameManager;
        private bool gameOver;
        private float currentTemperature;
        private float progress;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();

            currentTemperature = maxTemperature;

            StartCoroutine(TemperatureTick());

            player1.enabled = false;
            player2.enabled = false;

            enabled = false;

            transform.DOMove(Vector3.zero, 2f).SetEase(Ease.Linear).OnComplete(() =>
            {
                player1.enabled = true;
                player2.enabled = true;

                enabled = true;

                StartedPlaying?.Invoke();
            });
        }

        private void Update()
        {
            if (gameOver) return;

            progress = Mathf.Clamp01(progress + Time.deltaTime * progressPerSeconds);

            if (progress == 1f)
            {
                FullProgress();
            }
        }

        public abstract float TemperatureLossPerSecond();

        public float TemperatureLossForSecondPercentage()
        {
            return TemperatureLossPerSecond() / (float)maxTemperature;
        }

        public float CurrentTemperature()
        {
            return currentTemperature;
        }

        public int CurrentTemperaturePercentage()
        {
            return Mathf.RoundToInt((currentTemperature / maxTemperature) * 100);
        }

        public float CurrentProgressPercentage()
        {
            return progress;
        }

        protected void RaiseTemperatureReachedZero()
        {
            TemperatureReachedZero?.Invoke();
        }

        protected virtual void TemperatureAtZero()
        {
            player1.enabled = false;
            player2.enabled = false;

            gameOver = true;

            TemperatureReachedZero?.Invoke();
            Debug.Log("Game over...");

            transform.DOMove(gameOverMoveTransform.position, 2f).SetEase(Ease.Linear).OnComplete(() =>
            {

                gameManager.Defeat();
            });
        }

        private void FullProgress()
        {
            player1.enabled = false;
            player2.enabled = false;

            gameOver = true;

            ProgressCompleted?.Invoke();

            transform.DOMove(gameEndMoveTransform.position, 2f).SetEase(Ease.Linear).OnComplete(() => 
            {
                gameManager.Win();
            });
        }

        private IEnumerator TemperatureTick()
        {
            while (!gameOver)
            {
                yield return new WaitForSeconds(1);
                currentTemperature = Mathf.Clamp(currentTemperature - TemperatureLossPerSecond(), 0, maxTemperature);

                if (currentTemperature == 0)
                {
                    TemperatureAtZero();
                    yield break;
                }
            }
        }

        public int MaxTemperatureLossPerSecond => maxTemperatureLossPerSecond;
    }
}
