﻿using Game.Components.Ships;
using Game.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components
{
    [RequireComponent(typeof(Rigidbody))]
    public class Player : MonoBehaviour
    {
        [SerializeField] private PlayerInput input;
        [SerializeField] private float speed;
        [SerializeField] private float rubbleInteractionRange;

        private Gun gun;
        private Rigidbody rigidBody;
        private Collider[] overlapColliders;
        private Vector3 velocity;

        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            overlapColliders = new Collider[100];
        }

        private void Update()
        {
            var forward = (Input.GetKey(input.forwardPositive) ? 1 : 0) + (Input.GetKey(input.forwardNegative) ? -1 : 0);
            var right = (Input.GetKey(input.rightPositive) ? 1 : 0) + (Input.GetKey(input.rightNegative) ? -1 : 0);

            velocity = new Vector3(right, 0, forward).normalized * speed;

            // Switch to gun
            if (gun && gun.CanUse() && Input.GetKeyDown(input.interact))
            {
                enabled = false;

                StartCoroutine(Hacks.OnNextFixedUpdate(() =>
                {
                    gun.Use(this, input);
                    gameObject.SetActive(false);
                }));
            }

            var rubble = PollRubbleInRange();

            if (rubble)
            {
                if (Input.GetKeyDown(input.interact) || Input.GetKeyDown(input.fire))
                {
                    rubble.Collect();
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            gun = other.GetComponent<Gun>();
        }

        private void OnTriggerExit(Collider other)
        {
            if (gun == other.GetComponent<Gun>())
            {
                gun = null;
            }
        }

        private void FixedUpdate()
        {
            rigidBody.velocity = velocity;
        }

        private Rubble PollRubbleInRange()
        {
            var overlapCount = Physics.OverlapSphereNonAlloc(transform.position, rubbleInteractionRange, overlapColliders, LayerMask.GetMask("NoPlayerInteraction"));

            for (int i = 0; i < overlapCount; i++)
            {
                Collider overlappedCollider = overlapColliders[i];
                var rubble = overlappedCollider.GetComponent<Rubble>();

                if (rubble && rubble.Scattered) return rubble;
            }

            return default;
        }
    }
}
