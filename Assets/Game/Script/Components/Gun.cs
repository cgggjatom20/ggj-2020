﻿using DG.Tweening;
using Game.Components.Ships;
using Game.Models;
using System.Collections;
using UnityEngine;

namespace Game.Components
{
    [System.Serializable]
    public struct Cannon
    {
        public Transform cannonGun;
        public Transform initialPosition;
        public Transform kickbackPosition;
        public Transform muzzlePosition;
    }

    public class Gun : MonoBehaviour
    {
        [SerializeField] private Projectile projectile;
        [SerializeField] private Transform rotationRoot;
        [SerializeField] private float sensitivity;
        [SerializeField] private float reloadTime;
        [SerializeField] private float spread;
        [SerializeField] private int damage;
        [SerializeField] private Cannon[] cannons;

        private Ship ship;
        private PlayerInput input;
        private Player player;
        private Vector3 ejectPosition;
        private bool reloading;
        private bool firing;

        private void Awake()
        {
            ship = FindObjectOfType<Ship>();

            ship.ProgressCompleted += OnGameOver;
            ship.TemperatureReachedZero += OnGameOver;
        }

        private void Update()
        {
            // need a player and an input setting for the gun to work
            if (!input || !player) return;

            var rotationInput = (Input.GetKey(input.rightPositive) ? 1 : 0) + (Input.GetKey(input.rightNegative) ? -1 : 0);

            rotationRoot.rotation = rotationRoot.rotation * Quaternion.Euler(0, rotationInput * sensitivity, 0);

            // Eject player
            if (player && Input.GetKeyDown(input.interact))
            {
                enabled = false;

                StartCoroutine(Hacks.OnNextFixedUpdate(() =>
                {
                    player.enabled = true;

                    player.transform.position = ejectPosition;

                    player.gameObject.SetActive(true);

                    player = null;
                    input = null;
                }));
            }

            // Fire
            if (input && Input.GetKey(input.fire))
            {
                FireGuns();

                firing = true;
            }
            else
            {
                firing = false;
            }
        }

        private void OnDestroy()
        {
            if (ship)
            {
                ship.ProgressCompleted -= OnGameOver;
                ship.TemperatureReachedZero -= OnGameOver;
            }
        }

        public bool CanUse() => !enabled;

        public void Use(Player player, PlayerInput input)
        {
            if (enabled)    return;

            enabled = true;

            this.player = player;
            this.input = input;

            ejectPosition = this.player.transform.position;
            this.player.transform.position = new Vector3(rotationRoot.transform.position.x, this.player.transform.position.y, rotationRoot.transform.position.z);
        }

        private void FireGuns()
        {
            if (reloading) return;

            reloading = true;

            StartCoroutine(ReloadRoutine());
            StartCoroutine(FireRoutine());


            IEnumerator FireRoutine()
            {
                foreach (var cannon in cannons)
                {
                    var recoilTime = CannonFireAndRecoil(cannon);
                    yield return new WaitForSeconds(recoilTime);
                }
            }

            IEnumerator ReloadRoutine()
            {
                yield return new WaitForSeconds(reloadTime);
                reloading = false;
            }

            float CannonFireAndRecoil(Cannon cannon)
            {
                var spreadFavor = (rotationRoot.transform.right - cannon.muzzlePosition.transform.right).magnitude;
                var launchRandomness = Quaternion.Euler(0, Random.Range(-spread, spread) + spreadFavor, 0);

                // Launch projectile
                var projectileInstance = Instantiate(projectile, cannon.muzzlePosition.position, cannon.muzzlePosition.rotation * launchRandomness);
                projectileInstance.Launch(damage);

                // Recoil animation
                var recoilTime = Mathf.Min(reloadTime * 0.5f, 0.1f);

                cannon.cannonGun.DOLocalMove(cannon.kickbackPosition.localPosition, recoilTime)
                    .SetEase(Ease.OutSine)
                    .OnComplete(() =>
                    {
                        cannon.cannonGun.DOLocalMove(cannon.initialPosition.localPosition, recoilTime)
                            .SetEase(Ease.InSine);
                    });

                return recoilTime * 2;
            }
        }

        private void OnGameOver()
        {
            enabled = false;
        }
    }
}
