using Game.Components.Ships;
using System.Collections;
using UnityEngine;

namespace Game.Components
{
    public class MeteorSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject[] MeteorSpawnPoints;
        [SerializeField] private float InitialSpawnWaitTime;
        [SerializeField] private Vector2 SpawnTimeRange;
        [SerializeField] [Range(0, float.MaxValue)] private float InitialVelocity;
        [SerializeField] private float MeteorDestroyTime;
        [SerializeField] private int MinMeteorCount;
        [SerializeField] private int MaxMeteorCount;
        [SerializeField] private float MaxMeteorSpreadRadius;
        [SerializeField] private Meteor MeteorPrefab;
        [SerializeField] private int maxMeteorCount;

        private Ship ship;
        private Coroutine coroutine;
        private int meteorCount;

        private void Awake()
        {
            ship = FindObjectOfType<Ship>();

            ship.TemperatureReachedZero += StopSpawningOnTemperatureAtZeroAndFullProgress;
            ship.ProgressCompleted += StopSpawningOnTemperatureAtZeroAndFullProgress;

            Meteor.MeteorSpawned += OnMeteorSpawned;
            Meteor.MeteorDestroyed += OnMeteorDestroyed;
        }

        private void OnDestroy()
        {
            if (ship)
            {
                ship.TemperatureReachedZero -= StopSpawningOnTemperatureAtZeroAndFullProgress;
                ship.ProgressCompleted -= StopSpawningOnTemperatureAtZeroAndFullProgress;
            }

            Meteor.MeteorDestroyed -= OnMeteorDestroyed;
            Meteor.MeteorSpawned -= OnMeteorSpawned;
        }

        public void StartSpawning()
        {
            coroutine = StartCoroutine(SpawnRoutine());
        }

        public void StopSpawning()
        {
            if (coroutine != null)  StopCoroutine(coroutine);
        }

        private void SpawnRandom()
        {
            if (meteorCount >= maxMeteorCount)
                return;

            var nodeCount = Random.Range(0, MeteorSpawnPoints.Length);

            for (int i = 0; i < nodeCount; i++)
            {
                SpawnMeteor(MeteorSpawnPoints[i]);

                if (meteorCount >= maxMeteorCount)
                    return;
            }
        }

        private void SpawnMeteor(GameObject spawnPoint)
        {
            var meteorCount = Random.Range(MinMeteorCount, MaxMeteorCount);

            for (int _ = 0; _ < meteorCount; _++)
            {
                var meteorInstance = Instantiate(
                    MeteorPrefab,
                    spawnPoint.transform.position + ((Vector3.forward * Random.Range(-MaxMeteorSpreadRadius, MaxMeteorSpreadRadius)) + (Vector3.right * Random.Range(-MaxMeteorSpreadRadius, MaxMeteorSpreadRadius))),
                    spawnPoint.transform.rotation);

                meteorInstance.Launch(InitialVelocity, MeteorDestroyTime);
            }
        }

        private IEnumerator SpawnRoutine()
        {
            SpawnRandom();

            while (true)
            {
                yield return new WaitForSeconds(Random.Range(SpawnTimeRange.x, SpawnTimeRange.y));
                SpawnRandom();
            }
        }

        private void StopSpawningOnTemperatureAtZeroAndFullProgress()
        {
            if (coroutine == null) return;

            StopCoroutine(coroutine);
            coroutine = null;
        }

        private void OnMeteorSpawned()
        {
            meteorCount++;
        }

        private void OnMeteorDestroyed()
        {
            meteorCount = Mathf.Clamp(meteorCount - 1, 0, int.MaxValue);
        }
    }
}