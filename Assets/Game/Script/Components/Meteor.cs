using Game.Components.Ships;
using System;
using UnityEngine;

namespace Game.Components
{
    [RequireComponent(typeof(Target))]
    public class Meteor : MonoBehaviour
    {
        public static event Action MeteorSpawned;
        public static event Action MeteorDestroyed;

        private Target target;
        private Vector3 velocity;

        private void Awake()
        {
            target = GetComponent<Target>();

            MeteorSpawned?.Invoke();
        }

        private void FixedUpdate()
        {
            var rigidBody = gameObject.GetComponent<Rigidbody>();

            rigidBody.velocity = velocity;
        }

        private void OnDestroy()
        {
            MeteorDestroyed?.Invoke();
        }

        public void Launch(float velocity, float destroyTime)
        {
            this.velocity = transform.forward * velocity;

            Destroy(gameObject, destroyTime);
        }

        private void OnCollisionEnter(Collision collision)
        {
            var compartment = collision.collider.GetComponent<Compartment>();

            if (compartment)
            {
                target.FullDamage();
            }
        }
    }
}