﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components
{
    public class Target : MonoBehaviour
    {
        [SerializeField] private int health;
        [SerializeField] private GameObject explosionEffect;

        private int currentHealth;

        private void Awake()
        {
            currentHealth = health;
        }

        public void Damage(int damage)
        {
            currentHealth = Mathf.Clamp(currentHealth - damage, 0, health);

            // die
            if (currentHealth == 0)
            {
                CameraShake.Instance.Shake(1.65f);

                DestroyTarget();
                
                return;
            }
        }

        public void FullDamage()
        {
            CameraShake.Instance.Shake(2f);

            DestroyTarget();
        }

        private void DestroyTarget()
        {
            Destroy(gameObject);

            // spawn explosion particle
            var explosionEffectInstance = Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(explosionEffectInstance, 5f);

            // play explosion SFX
        }
    }
}
