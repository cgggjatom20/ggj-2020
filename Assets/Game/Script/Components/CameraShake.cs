﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components
{
    public class CameraShake : MonoBehaviour
    {
        private static CameraShake __instance;

        private Coroutine shakeRoutine;

        public static CameraShake Instance
        {
            get
            {
                if (!__instance)    __instance = FindObjectOfType<CameraShake>();

                return __instance;
            }
        }


        private void Update()
        {
            
        }

        public void Shake(float power)
        {
            if (shakeRoutine != null)           StopCoroutine(shakeRoutine);
            if (DOTween.IsTweening(transform))  transform.DOKill();

            shakeRoutine = StartCoroutine(ShakeRoutine());


            IEnumerator ShakeRoutine()
            {
                var firstShake = CreateShakeVector() * power;

                var deltaVector = firstShake - transform.localPosition;
                var remaining = 0.1f * (deltaVector.magnitude / firstShake.magnitude);

                while (remaining > 0f)
                {
                    transform.position += deltaVector * (Time.deltaTime / 0.1f);
                    remaining -= Time.deltaTime;
                    yield return null;
                }

                var secondShake = CreateShakeVector(firstShake) * power;

                deltaVector = secondShake - transform.localPosition;
                remaining = 0.1f * (deltaVector.magnitude / secondShake.magnitude);

                while (remaining > 0f)
                {
                    transform.position += deltaVector * (Time.deltaTime / 0.1f);
                    remaining -= Time.deltaTime;
                    yield return null;
                }

                transform.DOLocalMove(new Vector3(0, 10, 0), 0.1f);
            }
        }

        private Vector3 CreateShakeVector()
        {
            var horizontalShake = Mathf.PerlinNoise(Random.value, Random.value);
            var verticalShake = Mathf.PerlinNoise(Random.value, Random.value);

            return new Vector3(horizontalShake, 10, verticalShake);
        }

        private Vector3 CreateShakeVector(Vector3 previousShake)
        {
            var vector = CreateShakeVector();

            if (Quaternion.Angle(Quaternion.LookRotation(vector), Quaternion.LookRotation(previousShake)) < 90f)
            {
                var sign = Mathf.Sign(Random.Range(-1, 1));

                vector = Quaternion.Euler(0, sign * 90, 0) * vector;
            }

            return vector;
        }
    }
}
