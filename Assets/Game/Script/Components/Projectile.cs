﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Components
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private GameObject explosionEffect;
        [SerializeField] private float lifeTime;
        [SerializeField] private float speed;

        private Rigidbody body;
        private int damage;

        private void Awake()
        {
            body = GetComponent<Rigidbody>();

            body.velocity = transform.forward * speed;

            Destroy(gameObject, lifeTime);
        }

        private void OnTriggerEnter(Collider collider)
        {
            Destroy(gameObject);

            // Spawn explosion effect
            if (explosionEffect)
            {
                var explosionEffectInstance = Instantiate(explosionEffect, transform.position, Quaternion.identity);
                Destroy(explosionEffectInstance, 1f);
            }

            // Check target script, if it has a target script, damage it
            var target = collider.gameObject.GetComponent<Target>();
            if (target)     target.Damage(damage);
        }

        public void Launch(int damage)
        {
            this.damage = damage;
        }
    }
}
