﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Animations
{
    public class MainMenuAnimation : MonoBehaviour
    {
        [SerializeField] private Transform[] meteorRoots;
        [SerializeField] private Transform[] meteors;

        private void Awake()
        {
            foreach (var meteorRoot in meteorRoots)
            {
                meteorRoot.DORotate(new Vector3(0, Random.Range(2, 5), 0), 1).SetRelative(true).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
            }

            foreach (var meteor in meteors)
            {
                meteor.DORotate(new Vector3(3, 5, -2), 1).SetRelative(true).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
            }
        }
    }
}
