﻿using Game.Models;
using Game.Views;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Components
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private VictoryView victoryView;
        [SerializeField] private DefeatView defeatView;

        public void Win()
        {
            victoryView.gameObject.SetActive(true);
        }

        public void Defeat()
        {
            defeatView.gameObject.SetActive(true);
        }
    }
}
